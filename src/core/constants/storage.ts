
export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;


export const sucess_message = "Successfully added"
export const sucess_deleted_message = "Successfully deleted"
export const update_sucess_message = "Update successful"
export const status_sucess_message = "Successfully Changed"
export const error_message = "An error has occurred"
export const error_404_message = "Page not found"
export const unthorized_error_message = "Your session has expired. Please relogin"
export const invalid_credentials = "Invalid email or password"
export const email_is_already_taken = "Email is already taken"
export const save = "Save";
export const validate = "Validate";

export const currentUserKey = "currentUser";