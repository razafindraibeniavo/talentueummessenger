export default class Utils{

    static isUUIDValid = (input: string) => {
		const regexp = /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/
		return regexp.test(input);
	}
	/**
	 * Generate a radom unique id
	 * @returns {string} something like: "ec0c22fa-f909-48da-92cb-db17ecdb91c5"
	 */
	static uuid = () => {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	}

	/**
	 * Flat array of arrays
	 * @param arr
	 * @param d
	  @returns {}
	 */
	static flatDeep = (arr: any, d = 1) => {
		return d > 0 ? arr.reduce((acc: any, val: any) => acc.concat(Array.isArray(val) ? this.flatDeep(val, d - 1) : val), []) : arr.slice();
	};
}