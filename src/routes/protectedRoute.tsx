
import React, { useEffect, useState } from 'react';
import { connect, useDispatch } from 'react-redux';
import { Navigate, useLocation } from 'react-router-dom';
import User from '../models/all-export-model';
import { signOut } from '../pages/auth/auth-slice';
import { getUserProfile } from '../services/user';

const ProtectedRoute = (props: any) => {

  const [user, setUser] = useState<User>();
  const { isAuthed, allowedRole, children } = props;
  const location = useLocation();
  const dispatch = useDispatch();

  useEffect(() => {
    window.addEventListener("storage", () => {
      let userToken = sessionStorage.getItem('token');
      if (!userToken) {
        dispatch(signOut({}))
      }
    });
  }, []);

  if (!isAuthed) {
    return <Navigate to='/login' state={{ from: location }} />
  }
  if (allowedRole !== undefined ) {
    return <Navigate to='/messages' state={{ from: location }} />
  }
  return children;

};

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...ownProps,
    isAuthed: state.auth.authed
  }
}

export default connect(mapStateToProps)(ProtectedRoute);

