import { Modal, Form, Input, Select, Button } from "antd";
import { useEffect, useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import User from "../../models/all-export-model";
import { registerNewGroupThunk, setModalVisible } from "../message/message-slice";

const { Option } = Select;

const AddChannelModal = (props: any) => {
  const [formV] = Form.useForm();
  const dispatch = useDispatch();
  
  const { userListData, currentUserId } = props;
  const [formValue, setFormValue] = useState({
    form: { name: "", users: [] as unknown},
  });

  useEffect(() => {
      formV.setFieldsValue({
        name: "",
        users: [] as unknown
      });
      let { form } = formValue;
      form = {
        ...form,
        name: "",
        users: [],
      };
      setFormValue({ form });
  }, []);

  const isModalVisible = useSelector(
    (state: any) => state.message.isAddGroupModalVisible
  );

  //Change item on form
  const handleChange = (event: any) => {
    let { form } = formValue;
    form = { ...form, [event.target.id]: event.target.value };
    setFormValue({ form });
  };

  const handleSelectChange = (event: any) => {
    let form = formValue.form;
    form = { ...form, users: event };
    setFormValue({ form });
  };

  //AddChannelModal
  const sendDataAction = (e: any) => {
    e.preventDefault();
    formV.validateFields().then((values) => {
      let form = formValue.form;
      const userSelected:number[] = form.users as number[];
      form.users = [...userSelected, currentUserId]
      setFormValue({ form });

      dispatch(registerNewGroupThunk(form));
      dispatch(setModalVisible({}));
      formV.resetFields();
    });
  };

  //Cancel creation channel
  const handleCancel = () => {
    formV.resetFields();
    dispatch(setModalVisible({}));
  }; 
  
  return (
    <Modal
      title={"New Channel" }
      visible={isModalVisible}
      onCancel={handleCancel}
      footer={[
          <Button key="back" onClick={handleCancel}>
            Return
          </Button>,
          <Button className="btnCreateChannel" key="submit" type="primary" onClick={(e) => {
            sendDataAction(e);
          }}>
            OK
          </Button>
      ]}
    >
        
      <Form
        form={formV}
        layout="vertical"
      >
        
        <Form.Item
          name="users"
          label="Users"
          rules={[{ required: true, message: "Please select user!" }]}
        >
           
          <Select
            mode="multiple"
            id="users"
            allowClear={false}
            style={{ width: "100%" }}
            placeholder="select user"
            onChange={handleSelectChange}
            optionLabelProp="label"
          >
            {userListData !== undefined &&
            userListData.length > 0 ? (
                userListData.map((user: User) => {
                return (
                  <Option
                    key={user.id}
                    value={user.id}
                    label={user.firstname + " "+ user.lastname}
                    title={user.firstname + " "+ user.lastname}
                  >
                   {user.firstname + " "+ user.lastname}
                  </Option>
                );
              })
            ) : (
              <Option>{}</Option>
            )}
          </Select>
        </Form.Item>
        <Form.Item
          name="name"
          label="Name of Channel"
          rules={[{ required: true, message: "Please input the name of channel!" }]}
        >
          <Input
            size="large"
            id="name"
            placeholder="name"
            onChange={handleChange}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...ownProps
  };
};

export default connect(mapStateToProps)(AddChannelModal);
