import {createAsyncThunk, createEntityAdapter, createSlice, EntityState} from '@reduxjs/toolkit';

import { toast } from 'react-toastify';
import { currentUserKey } from '../../core/constants/storage';
import authAPI from '../../services/auth';
import { getUserProfile, registerUser } from '../../services/user';

export interface User{
	id?: number;
	email: string;
	password: string;
}

//Signin action
export const signInThunk = createAsyncThunk('auth/signIn', async (params:any) => {
	const user:User = params.user;
	const response = await authAPI.signIn(user?.email, user.password, params.usersListItems);
	return response;
});

//Sign up
export const signUpThunk = createAsyncThunk('auth/signUpThunk', async (user: User) => {
	// const response = await registerUser(user);
	return user;
});

//get current user connected info
export const getConnectedInfoThunk = createAsyncThunk('auth/myConnInfo', async () => {
	const response = await getUserProfile();
	return response;
});


export const authAdapter = createEntityAdapter();
const initialState = authAdapter.getInitialState({
	authed: false,
	signUpOnSuccess: "",
	loading: false,
	currentUser: null,
	currentToken:'', 
	usersListData: []  as User[],
	profileHasChanged: false});

export const authSlice = createSlice({
	name: 'auth',
	initialState,
	reducers: {
        signOut: (state, action) => {
            state.authed = false;
			state.currentUser = null;
			state.usersListData = [] as User[]
			sessionStorage.removeItem('token')
			sessionStorage.removeItem('authKey')
			sessionStorage.removeItem('currentUser')
			sessionStorage.clear()
			localStorage.removeItem(currentUserKey);
			localStorage.clear();
        },
		profileChanged: (state, action) => {
			state.profileHasChanged = true
		},
		resetProfileHasChanged: (state, action) => {
			state.profileHasChanged = false
		},
		resetInitValue: (state, action) => {
			state.signUpOnSuccess = ""
		}
	},
	extraReducers:(builder) => {
		//Sign In
		builder.addCase(signInThunk.pending, (state, action)=>{
			state.loading = true;
		 })
		 builder.addCase(signInThunk.fulfilled, (state, action)=> {
			state.currentToken = action.payload?.token ?? '';
			sessionStorage.setItem("authKey", state.currentToken ?? "");
			sessionStorage.setItem("token", JSON.stringify(state.currentToken ?? ""));
			if(!!state.currentToken){
				state.authed = true;
			}
			state.loading = false;
		 })
		 builder.addCase(getConnectedInfoThunk.fulfilled, (state, action)=>{
			state.currentUser = action.payload
		 })	 
	 
		 //Sign Up
		 builder.addCase(signUpThunk.pending, (state, action)=>{
			// state.loading = true;
		 })
		 builder.addCase(signUpThunk.fulfilled, (state, action)=> {
			state.signUpOnSuccess = "true";
			toast.success("Your account is created successfully, login now!")
			state.usersListData = [...state.usersListData, action.payload]
		 })
		 
		 builder.addCase(signUpThunk.rejected, (state, action)=> {
			state.signUpOnSuccess = "false";
		 })

	}
})

export const {signOut, profileChanged, resetProfileHasChanged, resetInitValue} = authSlice.actions;

export default authSlice.reducer;

export const authSelectors = authAdapter.getSelectors(
	(state: any) => state.auth
)


