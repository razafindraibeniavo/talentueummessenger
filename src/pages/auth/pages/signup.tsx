import React, { useEffect, useState } from "react";
import { Form, Input, Row, Card, Col, Anchor } from "antd";
import { useNavigate } from "react-router-dom";
import companyLogo from "../../../assets/img/logo.png";
import "./styles.css";

import { connect, useDispatch, useSelector } from "react-redux";


import { Layout } from "antd";
import { TextButton } from "../../../components/layout/text-button";
import { signUpThunk } from "../auth-slice";

const { Header, Content } = Layout;

export const SignUp= (props: any) => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const [formValue, setFormValue] = useState({form: {email: "", password:"", firstname: "", lastname: ""} })


  useEffect(() => {
    if (props.signUpOnSuccess === "true") {
      navigate("/signin");
    }
  }, [props.signUpOnSuccess]);

  //Action signup
  const signUp = () => {
    dispatch(signUpThunk(formValue.form))
  };

  //when change item value on the signup form
  const handleChange = (event: any) => { 
    let { form } = formValue
    form = {...form, [event.target.id]: event.target.value}
    setFormValue({form})
  }

  return (
    <Layout className="layout" style={{ height: "100vh" }}>
      <Header className="headerSlack">
        <div className="imageContainer">
          <img src={companyLogo} alt="Messenging" />
        </div>
      </Header>
      <Content className="contentSlack">
        <Row justify="center" align="middle">
          <Col xs={22}>
            <div
              className="container"
              style={{ maxWidth: "24rem", margin: "0 auto" }}
            >
              <div>
                <h1 style={{ textAlign: "center" }}>Messenging</h1>
              </div>
              <Card
                headStyle={{
                  borderBottom: 0,
                  textAlign: "center",
                  color: "#626262",
                  fontSize: "1.5rem",
                  letterSpacing: "-0.04em",
                }}
                title="Sign Up"
                bordered={true}
              >
                <Form
                  form={form}
                  name="basic"
                  style={{
                    backgroundColor: "#fff",
                    margin: 0,
                    padding: "1rem",
                  }}
                  layout="vertical"
                  initialValues={{ remember: true }}
                  onFinish={signUp}
                >
                  <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                      {
                        type: "email",
                        message: "Please input a valid email",
                      },
                      {
                        required: true,
                        message: "Your email is required",
                      },
                    ]}
                  >
                    <Input
                      size="large"
                      placeholder="Email"
                      id="email"
                      onChange = { handleChange }
                    />
                  </Form.Item>

                  <Form.Item
                    label="First Name"
                    name="firstname"
                    rules={[
                      {
                        required: true,
                        message: "Please input your first name!",
                      },
                    ]}
                  >
                    <Input
                      size="large"
                      id="firstname"
                      placeholder="Your first Name"
                      onChange = { handleChange }
                    />
                  </Form.Item>

                  <Form.Item
                    label="Last Name"
                    name="lastname"
                    rules={[
                      {
                        required: true,
                        message: "Please input your last name!",
                      },
                    ]}
                  >
                    <Input
                      size="large"
                      id="lastname"
                      placeholder="Your last Name"
                      onChange = { handleChange }
                    />
                  </Form.Item>

                  <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Please input your password!",
                      },
                    ]}
                  >
                    <Input.Password
                      type="password"
                      placeholder="*******"
                      size="large"
                      id="password"
                      onChange = { handleChange }
                    />
                  </Form.Item>
                  <Form.Item>
                    <TextButton type="submit">Sign Up</TextButton>
                  </Form.Item>
                </Form>
                <Row justify="end">
                  <Anchor>
                    <p className="pClickable"
                      onClick={() => navigate("/login")}
                    >
                    J'ai déjà un compte ? Se connecter 
                    </p>
                  </Anchor>
                </Row>
              </Card>
              {/* </div> */}
            </div>
          </Col>
        </Row>
      </Content>
    </Layout>
  );
};

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...ownProps,
    signUpOnSuccess: state.auth.signUpOnSuccess
  }
}

export default connect(mapStateToProps, null)(SignUp);
