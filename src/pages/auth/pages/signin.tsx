import React, { useEffect, useState } from "react";
import { Form, Input, Row, Card, Col } from "antd";
import { useNavigate} from "react-router-dom";
import companyLogo from "../../../assets/img/logo.png";
import "./styles.css";

import { connect, useDispatch, useSelector } from "react-redux";

import { Layout } from "antd";
import { TextButton } from "../../../components/layout/text-button";
import { getConnectedInfoThunk, signInThunk } from "../auth-slice";

const { Header, Content } = Layout;

export const SignIn: React.FC = () => {

    const dispatch = useDispatch();

    const navigate = useNavigate()

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const authState = useSelector((state:any) => state.auth.authed)
    const userListItems = useSelector((state:any) => state.auth.usersListData)

    //Action signin
    const signin = ()=>{
      dispatch(signInThunk({user:{email: email, password: password}, usersListItems:userListItems}));
      navigate("/messages");
    }  

    //Redirection if auth with success
    useEffect(() => {
      if(authState){
        dispatch(getConnectedInfoThunk())
        navigate("/messages");
      }
    }, [authState]);

  return (
    <Layout className="layout" style={{ height: "100vh" }}>
      <Header className="headerSlack">
        <div className="imageContainer">
          <img src={companyLogo} alt="Messenging" />
        </div>
      </Header>
      <Content className="contentSlack">
        <Row
          justify="center"
          align="middle"
        >
          <Col xs={22}>
            <div
              className="container"
              style={{ maxWidth: "24rem", margin: "0 auto" }}
            >
              <div>
                <h1 style={{ textAlign: "center" }}>Messenging</h1>
              </div>
              <Card
                headStyle={{
                  borderBottom: 0,
                  textAlign: "center",
                  color: "#626262",
                  fontSize: "1.5rem",
                  letterSpacing: "-0.04em",
                }}
                title="Sign in to your account"
                bordered={true}
              >
                <Form
                  name="basic"
                  style={{
                    backgroundColor: "#fff",
                    margin: 0,
                    padding: "1rem",
                  }}
                  layout="vertical"
                  initialValues={{ remember: true }}
                  onFinish={signin}
                  onFinishFailed={() => {}}
                  autoComplete="off"
                >
                  <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                      {
                        type: "email",
                        message: "Please input a valid email",
                      },
                      {
                        required: true,
                        message: "Your email is required",
                      },
                    ]}
                  >
                    <Input size="large" placeholder="Email" onChange={(e) => {
                        setEmail(e.target.value);
                      }}/>
                  </Form.Item>

                  <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Please input your password!",
                      },
                    ]}
                  >

                    <Input.Password
                      type="password"
                      placeholder="*******"
                      size="large" onChange={(e) => {
                        setPassword(e.target.value);
                      }}
                    />
                  </Form.Item>
                  <Form.Item>
                    <TextButton type="submit">Sign In</TextButton>
                  </Form.Item>
                </Form>
                <Row justify="end">
                    <p className="pClickable" onClick={() => navigate("/signup")}>  Pas encore de compte ? Créer un compte</p>
                </Row>
              </Card>
              {/* </div> */}
            </div>
          </Col>
        </Row>
      </Content>
    </Layout>
  );
};

export default connect() (SignIn) ;
