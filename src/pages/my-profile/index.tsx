import { useEffect, useState } from "react";
import {
  Form,
  Input,
  Row,
  Col,
} from "antd";
import { connect, useDispatch } from "react-redux";
import User from "../../models/all-export-model";
import { getUserProfile, updateUser } from "../../services/user";
import MainLayout from "../../components/main";
import { TextButton } from "../../components/layout/text-button";
import { profileChanged, resetProfileHasChanged } from "../auth/auth-slice";

const MyProfile = (props: any) => {

  const [form] = Form.useForm();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  const [hasUserDataChanged, setHasUserDataChanged] = useState(false);

  const [user, setUser] = useState<User>();
  const dispatch = useDispatch()

  const onSaveUserDetails = async (values: any) => {
    console.log("Received values of form:", values);
    await updateUser(user!.id, {
      firstname: values.firstName,
      lastname: values.lastName,
    })
      .then(() => {
          dispatch(profileChanged({}));
          setHasUserDataChanged(false)
        }
      )
      .catch((error) => console.error(error));
  };

  useEffect(() => {
    if(props.profileHasChanged){
      dispatch(resetProfileHasChanged({}));
    }
  }, [props.profileHasChanged])

  // useEffect(() => {
  //   getUserProfile()
  //     .then((res) => {
  //       setUser(res as User);
  //       form.setFieldsValue({
  //         email: res?.email,
  //         firstName: res?.firstname,
  //         lastName: res?.lastname,
  //       });
  //     })
  //     .catch((error) => {
  //       console.error(error);
  //     });

  // }, []);

  return (
    <MainLayout headerTitle={firstName + " " + lastName}>
      <div className="editUserContainer">
        <h2>Account Information </h2>

        <Form
          name="basic"
          form={form}
          style={{
            backgroundColor: "#fff",
            margin: 0,
            padding: "1rem",
          }}
          layout="vertical"
          onFinish={onSaveUserDetails}
          onFinishFailed={() => { }}
          autoComplete="off"
        >
          <Row gutter={[40, 24]}>
            <Col span={8}>
              <Form.Item label="Email" name="email">
                <Input
                  disabled
                  size="large"
                  placeholder="Your Email"
                  value={user?.email}
                />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={[40, 24]}>
            <Col span={8}>
              <Form.Item
                label="First Name"
                name="firstName"
                rules={[
                  {
                    required: true,
                    message: "Please input your first name!",
                  },
                ]}
              >
                <Input
                  size="large"
                  placeholder="Your first Name"
                  value={user?.firstname}
                  onChange={(e) => {
                    setFirstName(e.target.value);
                    setHasUserDataChanged(true);
                  }}
                />
              </Form.Item>
            </Col>

            <Col span={8}>
              <Form.Item
                label="Last Name"
                name="lastName"
                rules={[
                  {
                    required: true,
                    message: "Please input your last name!",
                  },
                ]}
              >
                <Input
                  size="large"
                  placeholder="Your last Name"
                  value={user?.lastname}
                  onChange={(e) => {
                    setLastName(e.target.value);
                    setHasUserDataChanged(true);
                  }}
                />
              </Form.Item>
            </Col>
          </Row>

          <Form.Item>
            <TextButton type="submit" disabled={!hasUserDataChanged}>
              Save
            </TextButton>
          </Form.Item>
        </Form>
      </div>
    </MainLayout>
  );
};


const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...ownProps,
    profileHasChanged: state.auth.profileHasChanged,

  }
}

export default connect(mapStateToProps, null) (MyProfile);
