
import { Navigate, useLocation } from 'react-router-dom';

export const NoMatch = () => {
  const location = useLocation();
  return <Navigate to='/messages' state={{ from: location }} />
  }
  