import React, { useEffect, useState } from "react";
import { Col, Layout, Menu, Row, Input , Badge} from "antd";
import MainLayout from "../../components/main";
import { connect, useDispatch, useSelector } from "react-redux";
import {
  getGroupConverstationThunk,
  getGroupInfoThunk,
  getUserConversationThunk,
  initializeUserConversation,
  resetInitValue,
  setModalVisible,
} from "./message-slice";
import styled from "styled-components";
import { Avatar } from 'antd';

import { toast } from "react-toastify";
import { SendOutlined, PlusOutlined } from "@ant-design/icons";
import moment from "moment";
import _ from "lodash";
import User, { Group } from "../../models/all-export-model";
import AddChannelModal from "../group/add-channel.modal";
const { Content, Footer } = Layout;

const DivUserCard = styled.div`
  background: white;
  border-radius: 5px;
  border: 1px solid #c7c3c6;
  padding: 0.25em 1em;
  margin-bottom: 0.5em;
`;

const MINUTE_MS = 2000

const Dashboard = (props: any) => {
  const dispatch = useDispatch();
  const { userList, groupList, currentUser, userConversation, newconversation, groupInfo, userListData } = props;
  const [newMessage, setNewMessage] = useState<string>("");
  const [userInGroup, setUserInGroup] = useState<string>("");
  const userListItems = useSelector((state:any) => state.auth.usersListData)
   const [currentMessage, setCurrentMessage] = useState<any>({});

  useEffect(() => {
    setUserInGroup("")
    dispatch(resetInitValue({}));
    dispatch(
      getUserConversationThunk()
    );
  }, []); 
    
  useEffect(() => {
    if(groupInfo !== undefined && groupInfo.length> 0) {
      const userString = Array.prototype.map
            .call(groupInfo, function (item: any) {
              return item.user.firstname + " " + item.user.lastname;
            })
            .join(", ");
      setUserInGroup(userString)     
    }
  }, [groupInfo]); 

  function setMessageStatus(userId:any) {
    // On prend l'ID de l'utilisateur connect dans le store auth.
    // Dans la liste de message, l'utilisateur (123456) sera l'utilisateur connecté
    // Son message sera stylisé en conséquence (fond bleu, mis à droite)    
    return userId == '123456' ? 'sent': 'received'
}

  const statusMessageStyle = (props:any) => {
    const status = setMessageStatus(props.userId)
    switch (status) {
        case 'sent':
            return 'preview-message--sent'
        case 'received':
            return 'preview-message--received'
        default:
            break;
    }
  }
  

  const selectUserAction = (user: User) => {
    dispatch(initializeUserConversation({}))
    setUserInGroup("")   
  };

  const selectGroupAction = (group: Group) => {
    dispatch(initializeUserConversation({}))
    setUserInGroup("") 

    dispatch(getGroupInfoThunk(group.id ?? 0));
    dispatch(
      getGroupConverstationThunk({
        groupid: group.id
      })
    );
  };

  const renderUserItem = (item: any) => (
    <Menu.Item key={item.id} className="side-menu-item" style={{
                backgroundColor: "#a2e5af",
               }}>
      <DivUserCard>
        <Row justify="space-between" align="middle">
           <Row onClick={(e) => selectUserAction(item)}> 
            <Col>
               <Row justify="space-between" align="middle">
                <Col> <Avatar src="https://joesch.moe/api/v1/random?key=1" />
                </Col>
                <Col>
                  <div>
                    <h3>{item?.firstname + " " + item?.lastname}</h3>
                  </div>
                </Col>
               </Row>
            </Col>
          </Row>
          <Row>
            <Col span={1}>
            { newconversation !== undefined && newconversation.length > 0  && newconversation.filter((it: any)=> it.recipientgroupid === 0 && it.message.creatorid === item.id).length > 0 && 
              <Badge count={1}> </Badge>} 
            </Col>
          </Row> 
        </Row>
      </DivUserCard>
    </Menu.Item>
  );
  const renderUserConversationItem = (currentMessage:any) => {
    if(Object.keys(currentMessage).length !== 0) {
      return currentMessage.content.map((item:any, index:any) => (
        <div key={index} className="itemMessage">
          <div className={"preview-message " + statusMessageStyle(item)}>
            {item.message}
          </div>
          <div className={"preview-message "+statusMessageStyle(item)} style={{ background: 'transparent' }}>
            <img className="message__picture" src={currentMessage.picture} alt=""/>
            <span className="message__date">{item.createdAt}</span>
          </div>
        </div>
      ));
    }
  };
  const renderUserConversationSideItem=(message:any)=>(
    <DivUserCard>
      <div className="messageSideItem" onClick={(e) => selectMessage(message)}>
        <div className="flex items-center flex-wrap">
            <Row justify="space-between" align="middle">
              <Col>
                <Row>
                <Col> <Avatar src="https://joesch.moe/api/v1/random?key=1" />
                </Col>
                <Col>
                  <div>
                    <h3>{ message.author }</h3>
                  </div>
                </Col>
              </Row>
              </Col>
              <Col>
                <span className="message__date message__date--right">{ extractTime(message.createdAt) }</span>
              </Col>
            </Row>
        </div>
        <div>
            <p className="message__text">
                {restrictLength(message.content[0].message) }
            </p>
        </div>
    </div>
  </DivUserCard>
  );

  
function selectMessage(message:any) {
    setCurrentMessage(message);
}

function extractTime(date:any) {        
    let createdDate =  moment(date)
    const now = moment()     
    const shiftInDay = now.diff(createdDate, 'day')        
    var result;
    if (shiftInDay < 1) {            
        result = createdDate.format('hh:mm')
    } else if (shiftInDay == 1) {
        result = 'Hier'
    } else if (shiftInDay > 1 && shiftInDay < 7) {
        result = createdDate.format('dddd')
    } else {
        result = createdDate.format('DD/MM/YYYY')
    }
    return result
}

  function restrictLength(str:any, n=50) {
    if (str.length > n) {
        return `${str.slice(0, n+1)}...`
    }
    return str
  }

  //send new message
  const sendMessage = (message:any) => {
      let currentDiscussion = _.cloneDeep(currentMessage)
      currentDiscussion.content.push({
        createdAt:moment().toLocaleString(),
        id:'xyz',
        message:newMessage,
        userId: '123457'
      })
      setCurrentMessage(currentDiscussion)
      toast.success("Message sent successfully!")
      setNewMessage("");
  };

  const onCreateNewChannel = () => {
    dispatch(setModalVisible({}));
  };

  return (
    <MainLayout>
      <Row>
       <div className="messageHeader">
          <h2>Messages</h2>
          <PlusOutlined style={{width:"25px",height:"25px"}}  onClick={onCreateNewChannel}/>
       </div>
      </Row>
      <Content>
        <div style={{display:"flex"}}>
         <Col flex="300px">
              <Row>
                <div >
                  {userConversation &&
                    userConversation.map(renderUserConversationSideItem)}
                </div>
              </Row>
          </Col>
        {Object.keys(currentMessage).length !== 0 && <Col flex="auto" >
            <Row>
              <div className="messageWrapper" style={{ }}>
                {renderUserConversationItem(currentMessage)}
              </div>
            </Row>
            <Row>
              <Footer
                style={{
                  bottom: 0,
                  width: "100%",
                }}
              >
                <div>
                  <Row justify="end" align="middle">
                    <Input
                        style={{
                          width: "80%",
                        }}
                        type="text"
                        value={newMessage}
                        className="input-send-message"
                        onChange={(e) => {
                          setNewMessage(e.target.value);
                        }}
                      />
                      <SendOutlined className="sendIcon" style={{}} onClick={sendMessage}/>
                  </Row>
                </div>
              </Footer>
            </Row>
          </Col>
        }
        </div>
        <AddChannelModal userListData={userListData} currentUserId={currentUser?.id} />
      </Content>
    </MainLayout>
  );
};

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...ownProps,
    userList: state.message.userList,
    currentUser: state.auth.currentUser,
    groupList: state.message.groupList,
    userConversation: state.message.userConversation,
    newconversation: state.message.newconversation,
    groupInfo: state.message.groupInfo,
    userListData: state.auth.usersListData
  };
};

export default connect(mapStateToProps, null)(Dashboard);
