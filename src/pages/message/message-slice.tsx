import {createAsyncThunk, createEntityAdapter, createSlice, EntityState} from '@reduxjs/toolkit';
import _ from "lodash";
import { getGroupConversation, getGroupInfo, getNotReadConversation, getUserConversation, registerNewGroup, sendMessage } from '../../services/message';
import {groupList, userList } from '../../services/user';

//get user list
export const getUserListThunk = createAsyncThunk('message/getUserListThunk', async () => {
	const response = await userList();
	return response;
})

//get all group list thunk
export const getGroupListThunk = createAsyncThunk('message/getGroupListThunk', async () => {
	const response = await groupList();
	return response;
});

//send new message
export const sendMessageThunk = createAsyncThunk('message/sendMessageThunk', async (dataRequest: any) => {
	const response = await sendMessage(dataRequest);
	return response;
});

//register new group
export const registerNewGroupThunk = createAsyncThunk('message/registerNewGroupThunk', async (dataRequest: any) => {
	const response = await registerNewGroup(dataRequest);
	return response;
});

//get user to user conversation
export const getUserConversationThunk = createAsyncThunk('message/getUserConversationThunk', async () => {
	const response = await getUserConversation();
	return response;
});

//Get group info
export const getGroupInfoThunk = createAsyncThunk('message/getGroupInfo', async (groupId: number) => {
	const response = await getGroupInfo(groupId);
	return response;
});

//Get group converstion
export const getGroupConverstationThunk = createAsyncThunk('message/getGroupConverstationThunk', async (dataRequest: any) => {
	const response = await getGroupConversation(dataRequest);
	return response;
});

//get all conversion not already read
export const getAllConversationNotReadThunk = createAsyncThunk('message/getAllConversationNotReadThunk', async (dataRequest: any) => {
	const response = await getNotReadConversation(dataRequest);
	return response;
});

export const messageAdapter = createEntityAdapter();
const initialState = messageAdapter.getInitialState({
	userList: [],
	groupList: [],
	userConversation: [],
	newconversation: [],
	groupInfo: [],
	isAddGroupModalVisible: false,
	loading: false,
});

export const messageSlice = createSlice({
	name: 'message',
	initialState,
	reducers: {
		resetInitValue: (state, action) => {
			state.userList = []
			state.groupList = []
			state.userConversation = []
			state.newconversation = []
			state.groupInfo = []
			state.isAddGroupModalVisible = false
			state.loading = false
		},
		setModalVisible: (state, action) => {
			state.isAddGroupModalVisible = !state.isAddGroupModalVisible;
		  },	
		initializeUserConversation: (state, action) => {
			state.userConversation = [];
			state.groupInfo = [];
		  },
	},
	extraReducers:(builder) => {
		//User List In
		builder.addCase(getUserListThunk.pending, (state, action)=>{
			state.loading = true;
		 })

		 builder.addCase(getUserListThunk.fulfilled, (state, action)=> {
			state.userList = action.payload;
			state.loading = false;
		 })

		 builder.addCase(getUserListThunk.rejected, (state, action)=>{
			state.userList = [];
			state.loading = false;
		 })	
		 
		 //Group List In
		builder.addCase(getGroupListThunk.pending, (state, action)=>{
			state.loading = true;
		 })
		 builder.addCase(getGroupListThunk.fulfilled, (state, action)=> {
			state.groupList = action.payload;
			state.loading = false;
		 })
		 builder.addCase(getGroupListThunk.rejected, (state, action)=>{
			state.loading = false;
		 })	 

		 //Send message In
		builder.addCase(sendMessageThunk.pending, (state, action)=>{
			state.loading = true;
		 })
		 builder.addCase(sendMessageThunk.fulfilled, (state, action)=> {
			let userConversationListCopy: any = _.cloneDeep(state.userConversation)
			userConversationListCopy = [
				...userConversationListCopy,
				action.payload,
			  ]
			state.userConversation = userConversationListCopy
			state.loading = false;
		 })

		 builder.addCase(sendMessageThunk.rejected, (state, action)=>{
			state.loading = false;
		 })	 
		 
		 //Register new group In
		builder.addCase(registerNewGroupThunk.pending, (state, action)=>{
			state.loading = true;
		 })
		 builder.addCase(registerNewGroupThunk.fulfilled, (state, action)=> {
			let groupListCopy: any = _.cloneDeep(state.groupList)
			groupListCopy = [
				...groupListCopy,
				action.payload,
			  ]
			state.groupList = groupListCopy;
		 })
		 builder.addCase(registerNewGroupThunk.rejected, (state, action)=>{
			state.loading = false;
		 })	 
		 
		 //Get user conversation
		builder.addCase(getUserConversationThunk.pending, (state, action)=>{
			state.loading = true;
		 })
		 builder.addCase(getUserConversationThunk.fulfilled, (state, action)=> {
			state.userConversation = action.payload
		 })
		 builder.addCase(getUserConversationThunk.rejected, (state, action)=>{
			state.loading = false;
		 })	 	
		 
		 //Get Group conversation
		builder.addCase(getGroupConverstationThunk.pending, (state, action)=>{
			state.loading = true;
		 })
		 builder.addCase(getGroupConverstationThunk.fulfilled, (state, action)=> {
			state.userConversation = action.payload
		 })
		 builder.addCase(getGroupConverstationThunk.rejected, (state, action)=>{
			state.loading = false;
		 })	 
		
		 //Get Unread conversation
		builder.addCase(getAllConversationNotReadThunk.pending, (state, action)=>{
			state.loading = true;
		 })
		 builder.addCase(getAllConversationNotReadThunk.fulfilled, (state, action)=> {
			state.newconversation = action.payload
		 })
		 builder.addCase(getAllConversationNotReadThunk.rejected, (state, action)=>{
			state.loading = false;
		 })	
		 
		 //Get group info
		builder.addCase(getGroupInfoThunk.pending, (state, action)=>{
			state.loading = true;
		 })
		 builder.addCase(getGroupInfoThunk.fulfilled, (state, action)=> {
			state.groupInfo = action.payload
		 })
		 builder.addCase(getGroupInfoThunk.rejected, (state, action)=>{
			state.loading = false;
		 })	 

	}
})

export const {resetInitValue, setModalVisible, initializeUserConversation} = messageSlice.actions;

export default messageSlice.reducer;


