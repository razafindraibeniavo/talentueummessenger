interface User {
    id: string;
    email: string;
    firstname: string;
    lastname: string;
    password?: string;
  }
  
export default User;
  
export interface Message{
  id?: number;
  creatorid: string;
  messagecontent: string;
}

export interface Group{
  id?: number;
  name: string;
}