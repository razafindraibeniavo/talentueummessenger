import axios from "axios";
import { toast } from "react-toastify";
import {
  API_BASE_URL,
  email_is_already_taken,
  error_404_message,
  error_message,
  invalid_credentials,
  unthorized_error_message,
} from "../core/constants/storage";

axios.defaults.baseURL = API_BASE_URL;
axios.defaults.headers.common["Accept"] = "application/json";

const axiosCustom = axios.create();

interface payload {
  [key: string]: unknown;
}

const authHeader = () => {
  return `Bearer ${sessionStorage.getItem("authKey")}`;
};

const rejectResponse = (e: any): Promise<any> =>
  new Promise((resolve, reject) => {
    let dataError = e.response.data.error;
    switch (dataError.statusCode) {
      case 401:
        if (dataError.message.includes('Error verifying token')) {
          sessionStorage.removeItem("token");
          window.dispatchEvent(new Event("storage"));
        } else if (dataError.message === "Invalid email or password.") {
          toast.error(invalid_credentials);
        } else {
          toast.error(error_message);
        }
        break;
      case 422:
        if (dataError.message === "Email is already taken") {
          toast.error(email_is_already_taken);
        } 
        else {
          toast.error(error_message);
        }
        break;
      case 404:
        toast.error(error_404_message);
        break;
      default:
        toast.error(error_message);
        break;
    }
    return reject(e.response.data.error.message);
  });

export const get = (path: string): Promise<any> =>
  new Promise((resolve, reject) => {
    axiosCustom
      .get(path, {
        headers: {
          Authorization: authHeader(),
        },
      })
      .then((res: any) => resolve(res?.data))
      .catch((e: any) => rejectResponse(e));
  });
  
export const getWithoutHeader = (path: string): Promise<any> =>
  new Promise((resolve, reject) => {
    axiosCustom
      .get(path)
      .then((res: any) => resolve(res?.data))
      .catch((e: any) => rejectResponse(e));
  });

export const post = (
  path: string,
  payload: payload | FormData,
  headers = {}
): Promise<any> =>
  new Promise((resolve, reject) => {
    axios
      .post(path, payload, {
        headers: {
          ...headers,
          Authorization: authHeader(),
        },
      })
      .then((res: any) => resolve(res?.data))
      .catch((e: any) => {
        rejectResponse(e);
      });
  });

export const postImages = (
  path: string,
  payload: payload | FormData,
  headers = {}
): Promise<any> =>
  new Promise((resolve, reject) => {
    axios
      .post(path, payload, {
        headers: {
          Authorization: authHeader(),
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res: any) => resolve(res?.data))
      .catch((e: any) => reject(e));
  });

export const patch = (path: string, payload: payload): Promise<any> =>

  new Promise((resolve, reject) => {
    axios
      .patch(path, payload, {
        headers: {
          Authorization: authHeader(),
        },
      })
      .then((res: any) => resolve(res?.data))
      .catch((e: any) => rejectResponse(e));
  });

export const put = (path: string, payload: payload): Promise<any> =>
  new Promise((resolve, reject) => {
    axios
      .put(path, payload, {
        headers: {
          Authorization: authHeader(),
        },
      })
      .then((res: any) => resolve(res?.data))
      .catch((e: any) => rejectResponse(e));
  });

export const del = (path: string, payload: payload = {}): Promise<any> =>
  new Promise((resolve, reject) => {
    axios
      .delete(path, {
        headers: {
          Authorization: authHeader(),
        },
      })
      .then((res: any) => resolve(res?.data))
      .catch((e: any) => rejectResponse(e));
  });

export const delWhere = (path: string): Promise<any> =>
  new Promise((resolve, reject) => {
    axios
      .delete(path, {
        headers: {
          Authorization: authHeader(),
        },
      })
      .then((res: any) => resolve(res?.data))
      .catch((e: any) => rejectResponse(e));
  });