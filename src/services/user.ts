import { get, patch, post } from "./axios-service";
import User from "../models/all-export-model";

//Mettre ici tout ce qui est requete axios

export async function userList() {
  const result = await get("users");
  return result;
}

export async function groupList() {
  const result = await get("groups");
  return result;
}

export async function registerUser(payload: any) {
  return await post("/users", payload);
}

export async function getUserById(userId: string) {
  return await get(`/users/${userId}`);
}

export async function updateUser(userId: string, payload: any) {
  return await patch(`/users/${userId}`, payload);
}

export async function getUserProfile() {
  return await get("/me");
}

export async function getUserDetails(userId: string) {
  return await get(`/userDetails/user/${userId}`);
}
