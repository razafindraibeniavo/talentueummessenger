import { get, patch, post, postImages } from "./axios-service";


const authAPI = {

	async signIn(email, password, currentUserList) {
    try{

        const userListItems =  currentUserList;
        console.log("User List values: ", userListItems)
        console.log("email: ", email)
        console.log("password: ", password)
        let token = null;
        for (let i = 0; i<currentUserList.length; i++) {
            if(currentUserList[i].email == email && currentUserList[i].password == password){
              token = (Math.random() + 1).toString(36)/*.substring(12)*/
              break;
            }
        }
        return {
          token: token
        };
        // const user = response;
        // sessionStorage.setItem("authKey", user.token);
        // localStorage.setItem("token", JSON.stringify(user.token));

        // return response;
    }catch (error) {
      throw new Error("INVALID LOGIN");
    }
	}
}

export default authAPI;