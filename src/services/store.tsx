import { configureStore } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import authReducer from "../pages/auth/auth-slice"
import messageReducer from "../pages/message/message-slice"
import { combineReducers } from 'redux';
import {
    persistReducer,
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const middleware: any[] = [
	/*YOUR CUSTOM MIDDLEWARES HERE*/
];

const persistConfig = {
    key: 'root',
    storage,
};

const rootReducer = combineReducers({
    auth: persistReducer(persistConfig, authReducer),
    message: messageReducer
  })

export default configureStore({
    reducer: rootReducer,
    middleware: [...middleware, thunk]
});