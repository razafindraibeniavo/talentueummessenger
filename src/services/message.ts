import { del, get, patch, post } from "./axios-service";

//Mettre ici tout ce qui est requete axios du menu message


export async function sendMessage(payload: any) {
  const result = await post("messages", payload);
  return result;
}

export async function registerNewGroup(payload: any) {
  const result = await post("groups", payload);
  return result;
}

export async function getGroupInfo(groupId: any) {
  let query: object = {
    where: {
      groupId: groupId
    },
  };
  const result = await get("usergroups?filter="+JSON.stringify(query));
  return result;
}

export async function getUserConversation() {
        console.log('--------------------> Fetch');
        const resp = await get('https://576cc2bd-e943-49e1-8075-1f9a1ede52a8.mock.pstmn.io/messages')
        const messages = resp
        messages.push({
            "_id": "123456789",
            "picture": "https://res.cloudinary.com/dkthctbvw/image/upload/v1671190678/logo_google_eifp5d.png",
            "author": "Niavo",
            "content": [
                {
                "id": "639c93ccdf008a7f6e088e21",
                "message": "Dolor nulla sint eu in aute consequat voluptate elit. Officia cillum nulla tempor nostrud id. Consectetur adipisicing veniam et non sint velit. Commodo nostrud nostrud commodo qui ad sit incididunt irure aliqua sint laboris sint nisi sit. Tempor eiusmod ipsum esse consequat consectetur occaecat duis sint aliquip. Dolor nulla sint eu in aute consequat voluptate elit. Officia cillum nulla tempor nostrud id. Consectetur adipisicing veniam et non sint velit. Commodo nostrud nostrud commodo qui ad sit incididunt irure aliqua sint laboris sint nisi sit. Tempor eiusmod ipsum esse consequat consectetur occaecat duis sint aliquip. Dolor nulla sint eu in aute consequat voluptate elit. Officia cillum nulla tempor nostrud id. Consectetur adipisicing veniam et non sint velit. Commodo nostrud nostrud commodo qui ad sit incididunt irure aliqua sint laboris sint nisi sit. Tempor eiusmod ipsum esse consequat consectetur occaecat duis sint aliquip. Dolor nulla sint eu in aute consequat voluptate elit. Officia cillum nulla tempor nostrud id. Consectetur adipisicing veniam et non sint velit. Commodo nostrud nostrud commodo qui ad sit incididunt irure aliqua sint laboris sint nisi sit. Tempor eiusmod ipsum esse consequat consectetur occaecat duis sint aliquip.",
                "createdAt": "Tue Aug 26 2014 07:42:08 GMT+0300 (East Africa Time)",
                "userId": "123456"
                },
                {
                "id": "639c93ccc34b7b04c5604709",
                "message": "Consectetur ex dolor incididunt voluptate deserunt adipisicing sint deserunt eu reprehenderit officia nostrud laborum. Officia ipsum non in proident veniam. Excepteur ipsum laboris est pariatur qui sint consequat ea non pariatur labore anim labore. Non ex nisi sint officia exercitation mollit. Et quis id veniam proident.",
                "createdAt": "Mon Jun 25 2018 13:12:13 GMT+0300 (East Africa Time)",
                "userId": "123457"
                }
            ],
            "createdAt": "Mon Fev 27 2022 12:00:31 GMT+0300 (East Africa Time)",
            "status": "OPEN"
        })
       return messages;
}

export async function getGroupConversation(queryRequest: any) {
  let query: object = {
    where: {
      groupid: queryRequest.groupid
    },
    
  };
  const result = await get("messagerecipients?filter="+JSON.stringify(query));
  return result;
}

export async function getNotReadConversation(queryRequest: any) {
  const result = await get("messagerecipients/notready/"+queryRequest.userid);
  return result;
}
