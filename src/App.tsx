import * as React from "react";
import {
  Routes,
  Route,
  BrowserRouter,
} from "react-router-dom";
import "antd/dist/antd.less";
import "./styles/main.scss";
import moment from "moment";
import { GlobalContextProvider } from "./contexts";
import { createBrowserHistory } from "history";

import { Provider } from "react-redux";

import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';

import { Layout } from "antd";
import store from "./services/store";
import { ToastContainer } from "react-toastify";
import { StyledThemeProvider } from "./definitions/styled-components";
import { Header } from "./components/layout/header";
import SiderBar  from "./components/layout/sider";
import ProtectedRoute from "./routes/protectedRoute";
import { NoMatch } from "./pages/noMatch";
import MyProfile from "./pages/my-profile";
import Dashboard from "./pages/message";
import 'react-toastify/dist/ReactToastify.css';
import { SignIn } from "./pages/auth/pages/signin";
import { SignUp } from "./pages/auth/pages/signup";
//...
let persistor = persistStore(store);

moment.updateLocale('en', {
  week: {
    dow: 1,
  },
})
export const appHistory = createBrowserHistory();

const App: React.FC = () => {
  
  return (
    <Provider store={store}>
      <GlobalContextProvider>
      <ToastContainer
            position="bottom-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            pauseOnFocusLoss
            draggable
            pauseOnHover
            />
        <PersistGate persistor={persistor}>
          <StyledThemeProvider>
            <BrowserRouter>
              <Header />
                <Layout>
                  {/* <SiderBar /> */}
                    <Routes>
                        <Route path="/login" element={<SignIn/>}/>
                        <Route
                            path="/signup"
                            element={
                                <SignUp/>
                              }
                          />
                        <Route 
                              path='/messages' 
                              element={
                                <ProtectedRoute>
                                  <Dashboard />
                                </ProtectedRoute>
                              }
                            />
                          <Route
                            path="/myProfile"
                            element={
                              <ProtectedRoute>
                                <MyProfile/>
                              </ProtectedRoute>
                              }
                          /> 
                          <Route path='*' element={<NoMatch />} />
                  </Routes>
              </Layout>
            </BrowserRouter>
          </StyledThemeProvider>
        </PersistGate>
      </GlobalContextProvider>
    </Provider>
  );
}

export default App;
