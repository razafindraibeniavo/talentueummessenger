import { useState, createContext } from "react";

export const GlobalContext = createContext({
  role: "",
  setRole: (value: string)=>{}
})

export const GlobalContextProvider = (props: any) => {
  const { children } = props;
  const [role, setRole] = useState(localStorage.getItem("role") ?? "");

  const clearRole = () => {
    setRole("");
    localStorage.removeItem("role");
  };

  const handleSetRole = (param: string) => {
    setRole(param || "");
    localStorage.setItem("role", param ? param : "");
  };

  const contextValue = 
    {
      role,
      setRole: handleSetRole,
      clearRole
  };

  return (
    <GlobalContext.Provider value={contextValue}>
      {children}
    </GlobalContext.Provider>
  );
};
