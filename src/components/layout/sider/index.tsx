import { Layout, Menu } from "antd";

import { NavLink } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { connect } from "react-redux";

const { Sider } = Layout;

 const SiderBar = (props: any) => {
  const location = useLocation();

  const menuItems = [
    {
      key: "messages",
      label: "Messages",
      link: "/messages",
    },
    {
      key: "myprofile",
      label: "My profile",
      link: "/myprofile",
    }
  ]

  const renderMenuItem = (item: any) => (
    <Menu.Item key={item.key} className="side-menu-item">
      <NavLink to={item.link}>
        <div className="menu-item-text-holder">
          <span className="menu-item-text">{item.label}</span>
        </div>
      </NavLink>
    </Menu.Item>
  );

  const getCurrentLocation = () => {
    
    const arrayLocationKey = [ "myprofile", "messages"];
    for (let index = 0; index < arrayLocationKey.length; index++) {
      if(location.pathname.indexOf(arrayLocationKey[index]) !== -1){
        return arrayLocationKey[index];
      }
    }
    if(location.pathname.includes("/myProfile")){
      return "";
    }
    return "messages";
  }


  const handleSelectedMenuItemClicked = ( key : any) => {
    sessionStorage.setItem("selectedMenuItem", key.key);
  };

  const renderMenuItems = (items: any[]) => items.map(renderMenuItem);

  return (
    props.isAuthed ? 
      ( <Sider trigger={null} collapsible collapsed={false}>
          <Menu
            theme="dark"
            selectedKeys={[ getCurrentLocation()]}
            mode="inline"
            onClick={handleSelectedMenuItemClicked}
          >
            {renderMenuItems(menuItems)}
          </Menu>
        </Sider>
      )
     :
     <div></div>
  );
};


const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...ownProps, 
    isAuthed: state.auth.authed
  }
}

export default connect(mapStateToProps, null) (SiderBar);
