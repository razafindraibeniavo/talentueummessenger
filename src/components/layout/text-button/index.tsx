import React from "react";
import { Button } from "antd";
import { ButtonProps } from "antd/lib/button";
import styled from "styled-components";

export type ITextButton = ButtonProps;

export const TextButton = styled(
  ({
    padding,
    rounded,
    uppercase,
    width,
    type,
    variant,
    hoverBrightness,
    lineHeight,
    style,
    ...rest
  }) => {
    return <Button style={style || {}} htmlType={type} type="primary" size="large" shape="round" {...rest} data-testid="btn" />;
  }
)`
  margin: 1rem 0;
  text-transform: ${({ uppercase }) => (uppercase ? "uppercase" : "none")};
  width: ${(props) => props.width || "20.25rem"};
  padding: ${(props) =>  ( props.width === "auto" ? "0rem 5rem !important" : 0)}
`;
