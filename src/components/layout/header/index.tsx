import { Row } from "antd";
import styled from "styled-components";
import { useSelector } from "react-redux";
import companyLogo from "../../../assets/img/logo.png";
import DropDownAvatar from "./dropdown-avatar";

export const Header = styled(({ children, ...rest }) => {

  const isAuthed =  useSelector((state : any) => state.auth.authed )

  return (
    isAuthed ? 
    ( <Row
      {...rest}
      justify="space-between"
      align="middle"
      data-testid="container"
    >
      <img src={companyLogo} alt="Messaging clone" className="logoImg"/>
      {/* {user && <h1 className="userName">{user?.firstname + " " + user?.lastname}</h1>} */}
      <h1 className="title">Messaging</h1>
      <DropDownAvatar/>
    </Row> ) 
    : <div></div>
  );
})`
  box-shadow: var(--box-shadow);
  background: white};
  height: var(--header-height);
  gap: 1rem;
  position: sticky;
  top: 0;
  z-index: 12;
  padding: 1rem;

  .title {
    padding: 0;
    margin: 0;
  }

  .logoImg{
    height:40px
  }
`;
