import { Avatar, Dropdown, Menu, Row } from "antd";
import styled from "styled-components";
import { connect, useDispatch } from "react-redux";
import { UserOutlined } from "@ant-design/icons";
import { useState, useEffect } from "react";
import { getUserProfile } from "../../../../services/user";
import { signOut } from "../../../../pages/auth/auth-slice";
import User from "../../../../models/all-export-model";

const PUsername = styled.p`
   margin: 1rem;
`;

const DropDownAvatar = (props : any) => {

  const [user, setUser] = useState<User>();

  useEffect(() => {
    if(props.isAuthed){
    }
  }, [props.isAuthed]);

  useEffect(() => {
    if(props.profileHasChanged){
      getUserProfile()
      .then((res: User) => {
        setUser(res as User);
      })
      .catch((error: any) => {
        console.error(error);
      });
    }
  }, [props.profileHasChanged])
  
  const dispatch = useDispatch();
  
  const signOutAction = () => {
    dispatch(signOut({}));
  };

  const userMenu = (
    <Menu>
      <Menu.Divider />
      <Menu.Item key="3" onClick={signOutAction}>
        Sign out
      </Menu.Item>
    </Menu>
  );

  return (
    <Dropdown className="dropdown-btn button" overlay={userMenu}>
      <Row>
        <PUsername>{user?.firstname}</PUsername>
        <Avatar size="large" icon={<UserOutlined></UserOutlined>}></Avatar>
      </Row>
    </Dropdown>
  );
};

const mapStateToProps = (state: any, ownProps: any) => {
  return {
    ...ownProps, 
    profileHasChanged: state.auth.profileHasChanged,
    isAuthed: state.auth.authed,
    currentUser: state.auth.currentUser,
  }
}

export default connect(mapStateToProps, null) (DropDownAvatar);