import { connect, useSelector } from "react-redux";
import {
    Navigate
  } from "react-router-dom";

const AuthRoute = (props:any) => {
    const authState = useSelector((state:any) => state.auth.authed)
    if (!authState) {
        return props.children
      } else {
        return (
          <Navigate
            to={{
              pathname: props.path,
            }}
          />
        )
      }
}

export default connect() (AuthRoute)

