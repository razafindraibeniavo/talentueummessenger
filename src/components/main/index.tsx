
import { Layout } from "antd";
import './index.css'
const { Content } = Layout;


const MainLayout = (props: any) => {
    const {current} = props;
    const { children } = props;
    return (
        <>
            <Layout>
                <Layout id="main-layout">
                    <Content className="main-content">{children}</Content>
                </Layout>
            </Layout>
        </>
    )
}

export default MainLayout;